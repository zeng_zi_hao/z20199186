#include<stdio.h>
 int menu();
 int error();
 int help();
 int grade1(int a);
 int grade2(int a);
 int grade3(int a);
 int main ()
 {
 	int n,m=0;
 	printf("========== 口算生成器 ==========\n");
 		printf("欢迎使用口算生成器 :\n");
 		printf("\n");
 		help();
 		printf("\n");
 		printf("\n");
 		menu();
 	while(m!=1)
 	{
 		scanf("%d",&n);
 		switch(n)
		 {
		 	case 1:grade1(n); break;
		 	case 2:grade2(n); break;
		 	case 3:grade3(n); break;
		 	case 4:help(); break;
		 	case 5:m=1;printf("程序结束, 欢迎下次使用\n任意键结束……"); break;
		 	default:error(); break;
		}
	}
	return 0;
 }
 int menu()
 {
 	printf("操作列表:\n");
 	printf("1)一年级    2)二年级    3)三年级\n");
 	printf("4)帮助      5)退出程序\n");
 	printf("请输入操作> \n");
 }
 int error()
 {
 	printf("Error!!!\n");
 	printf("错误操作指令, 请重新输入\n");
 }
 int help()
 {
 	printf("帮助信息\n");
 	printf("您需要输入命令代号来进行操作, 且\n");
 	printf("一年级题目为不超过十位的加减法;\n");
 	printf("二年级题目为不超过百位的乘除法;\n");
 	printf("三年级题目为不超过百位的加减乘除混合题目.\n");
 }
 int grade1(int a)
 {
 	int n,m,b,i;char op;  
 	printf("< 执行操作 :\n");
	printf("现在是一年级的题目\n");
	printf("请输入生成个数>");
	scanf("%d",&a); 
	for(i=1;i<=a;i++)
	{
		b=rand() % 2;
		if(b==0)
		op='+';
		if(b==1)
		op='-';
		m=rand()%10;
		n=rand()%10;
		printf("%d %c %d = ___\n",m,op,n);
	}
	printf("执行完了\n");
 }
 int grade2(int a)
 {
 	printf("< 执行操作 :\n");
	printf("现在是二年级的题目\n");
	printf("请输入生成个数>");
	int n,m,b,i;char op;
	scanf("%d",&a);
	for(i=1;i<=a;i++)
	{
		b=rand()%2;
		if(b==0)
		op='*';
		if(b==1)
		op='/';
		m=rand()%10;
		n=rand()%10;
		if(n==0&&op=='/')
		n=rand()%9+1;
		printf("%d %c %d = ___\n",m,op,n);	
	}
		printf("执行完了\n");
 }
 int grade3(int a)
 {
 	printf("< 执行操作 :\n");
	printf("现在是三年级的题目\n");
	printf("请输入生成个数>");
	int n,m,b1,b2,c,i;char op1,op2;
		scanf("%d",&a);
	for(i=1;i<=a;i++)
	{
		b1=rand()%4;
		if(b1==0)
		op1='*';
		if(b1==1)
		op1='/';
		if(b1==2)
		op1='+';
		if(b1==3)
		op1='-';
		m=rand()%100;
		n=rand()%100;
		if(op1=='/'&&n==0)
		n=rand()%99+1;
		b2=rand()%4;
		if(b2==0)
		op2='*';
		if(b2==1)
		op2='/';
		if(b2==2)
		op2='+';
		if(b2==3)
		op2='-';
		c=rand()%100;
		if(op2=='/'&&c==0)
		c=rand()%99+1;
		printf("%d %c %d %c %d = ___\n",m,op1,n,op2,c);
	} 
	printf("执行完了\n");
  } 
