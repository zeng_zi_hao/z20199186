#include<stdio.h>
 int menu();
 int error();
 int help();
 int grade1(int a);
 int grade2(int a);
 int grade3(int a);
 int main ()
 {
 	int n,m=0;
 	printf("========== 口算生成器 ==========\n");
 		printf("欢迎使用口算生成器 :\n");
 		printf("\n");
 		help();
 		printf("\n");
 		printf("\n");
 		menu();
 	while(m!=1)
 	{
 		scanf("%d",&n);
 		switch(n)
		 {
		 	case 1:grade1(n); break;
		 	case 2:grade2(n); break;
		 	case 3:grade3(n); break;
		 	case 4:help(); break;
		 	case 5:m=1;printf("程序结束, 欢迎下次使用\n任意键结束……"); break;
		 	default:error(); break;
		}
	}
	return 0;
 }
 int menu()
 {
 	printf("操作列表:\n");
 	printf("1)一年级    2)二年级    3)三年级\n");
 	printf("4)帮助      5)退出程序\n");
 	printf("请输入操作> \n");
 }
 int error()
 {
 	printf("Error!!!\n");
 	printf("错误操作指令, 请重新输入\n");
 }
 int help()
 {
 	printf("帮助信息\n");
 	printf("您需要输入命令代号来进行操作, 且\n");
 	printf("一年级题目为不超过十位的加减法;\n");
 	printf("二年级题目为不超过百位的乘除法;\n");
 	printf("三年级题目为不超过百位的加减乘除混合题目.\n");
 }
 int grade1(int a)
 {
 	printf("< 执行操作 :\n");
	printf("现在是一年级的题目\n");
	printf("执行完了\n");
 }
 int grade2(int a)
 {
 	printf("< 执行操作 :\n");
	printf("现在是二年级的题目\n");
	printf("执行完了\n");
 }
 int grade3(int a)
 {
 	printf("< 执行操作 :\n");
	printf("现在是三年级的题目\n");
	printf("执行完了\n");
  } 
